"""
    This file is part of GAS-site.
    Copyright (C) 2016 Saikiran Srirangapalli <saikiran1096@gmail.com>

    GAS-site is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GAS-site is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GAS-site.  If not, see <http://www.gnu.org/licenses/>.
"""
from tempfile import NamedTemporaryFile
from subprocess import Popen, PIPE
import re
import gradaudit.util as util


class ExecutionError(Exception):
    """ Thrown when there is an error executing main.lp under s(ASP) """

    def __init__(self, msg, main, err):
        self.msg = msg
        self.main = main
        self.err = err
        self.strepr = '{}\n'\
                      'Main contents:\n{}\n'\
                      'Error:\n{}'.format(msg, main, err)

    def __str__(self):
        return self.strepr


class GASResponse(object):
    """ GAS advising information """
    _missing_class_re = r'hasNotTaken\({},([a-z]+[1-4][0-9V][0-9][0-9])\)'
    _missing_hours_re = r'needsHours\({},([^)]+),([0-9]+)\)'
    _missing_electives_re = r'needsElectiveHours\({},([0-9]+)\)'
    _grad = 'ableToGraduate({})'

    def __init__(self, transcript):
        self.transcript = transcript

        main_contents = util.gen_main(transcript)
        answer_set = GASResponse._run(main_contents).decode("utf-8")
        advice = GASResponse._extract_advice(answer_set, transcript.name)

        self.missing_classes = advice[0]
        self.missing_hours = advice[1]
        self.missing_electives = advice[2]
        self.can_grad = advice[3]

    @classmethod
    def _run(cls, main_contents):
        """ Run the program under s(ASP) and return the answer set """
        with NamedTemporaryFile() as fil:
            fil.write(main_contents.encode("utf-8"))
            fil.flush()

            # Command to run audit
            audit = ['sasp', fil.name]

            # Execute the audit, capture stdout, stderr
            run = Popen(audit, stdout=PIPE, stderr=PIPE)
            (answer_set, audit_err) = run.communicate()

            if run.returncode != 0:
                msg = 'Error executing audit'
                raise ExecutionError(msg, main_contents, audit_err)

        return answer_set

    @classmethod
    def _extract_advice(cls, answer_set, name):
        """ Parses answer set for advising information """
        class_re = cls._missing_class_re.format(name)
        hours_re = cls._missing_hours_re.format(name)
        electives_re = cls._missing_electives_re.format(name)
        grad = cls._grad.format(name)

        classes = re.findall(class_re, answer_set)
        hours = re.findall(hours_re, answer_set)

        electives = re.findall(electives_re, answer_set)
        if electives == []:
            electives = 0
        else:
            electives = int(electives[0])

        can_grad = grad in answer_set

        return (classes, hours, electives, can_grad)
