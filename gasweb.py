"""
    This file is part of GAS-site.
    Copyright (C) 2016 Saikiran Srirangapalli <saikiran1096@gmail.com>

    GAS-site is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    GAS-site is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with GAS-site.  If not, see <http://www.gnu.org/licenses/>.
"""
import gradaudit.util as util
import gradaudit.extract as extract
import re
from flask import Flask, render_template, request
from gasresponse import GASResponse
from tempfile import NamedTemporaryFile

app = Flask(__name__)


@app.route('/', methods=['POST', 'GET'])
def input_form():
    if request.method == 'GET':
        return render_template('home.html')

    uploaded = request.files['transcriptPDF']
    with NamedTemporaryFile() as fil:
        uploaded.save(fil)
        fil.flush()
        course_data = extract.extract_courses(fil.name)

    courses = map(lambda x: x[0], course_data)
    courses = ','.join(courses)
    return render_template('home.html', classes=courses)


@app.route('/submit', methods=['POST'])
def submit():
    name = 'student'
    degree = request.form['degree_type']
    major = request.form['major']
    year = request.form['year']
    spec = request.form['specialization'].lower()

    classes = request.form['classes']
    classes = re.sub(r'\s+', '', classes)
    classes = classes.rstrip(',')
    classes = classes.split(',')

    if classes == ['']:
        classes = []

    transcript = util.Transcript(
        name, degree, major, year, spec, classes)

    resp = GASResponse(transcript)

    return render_template('results.html', response=resp)


@app.errorhandler(500)
def exc(err):
    error_list = str(err).split("\n")
    return render_template('error.html', error_message=error_list)
